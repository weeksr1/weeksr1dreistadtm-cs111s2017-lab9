import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            else if(command.equals("priority-search")) {
                System.out.println("What is the priority?");
                String requestedPriority = scanner.nextLine();
                Iterator priorityIterator = todoList.findTasksOfPriority(requestedPriority);
                while(priorityIterator.hasNext()) {
                    System.out.println((TodoItem)priorityIterator.next());
                }
            }
            else if(command.equals("category-search")) {
                System.out.println("What is the category?");
                String requestedCategory = scanner.nextLine();
                Iterator categoryIterator = todoList.findTasksOfCategory(requestedCategory);
                while(categoryIterator.hasNext()) {
                    System.out.println((TodoItem)categoryIterator.next());
                }
            }
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            else if(command.equals("quit")) {
                break;
            }
        }

    }

}
